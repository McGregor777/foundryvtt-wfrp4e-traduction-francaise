# FoundryVTT - WFRP4e - Traduction française

Ce module est une traduction maison française du système de jeu pour WFRP4 de Moo Man.

Elle vise à traduire tout le texte depuis le matériau original, ce qui inclu : 
 * L'interface et les fiches,
 * Les tables,
 * Et les compendiums.

Afin de fluidifier l'accès aux pléthores d'éléments proposés dans les modules officiels, toutes les descriptions sont améliorées par l'implémentation de davantage de liens vers d'autres éléments.

Le nom des différentes Compétences, Talents, Carrières, Sorts et Objets peuvent ne pas correspondre à ceux attribués dans la traduction officielle par Khaos Project pour des raisons de justesse vis-à-vis des noms originaux et de plaisir de lecture. 

## Installation

Pour installer ce module, merci suivre les instructions suivantes :
 * Depuis le menu principal de FoundryVTT, rendez-vous dans l'onglet "Add-on Modules".
 * Copiez cette URL dans votre presse-papier : https://gitlab.com/McGregor777/foundryvtt-wfrp4e-traduction-francaise/-/blob/6e5e708a32790ae81b35409c56bdf085aebc2190/module.json.
 * Collez l'URL dans la barre d'entrée en bas de la fênetre intitulée "Manifest URL:", puis cliquez sur le bouton "Install".
 * Lancez votre monde depuis l'onglet "Game Worlds".
 * Une fois connecté, rendez-vous dans l'onglet "Game Settings" du panneau de droite.
 * Ouvrez le menu "Module Management" en cliquant sur le bouton "Manage Modules".
 * Cherchez le module "WFRP4e - Traduction Française" puis cochez la case précédant l'intitulé avant de cliquez sur le bouton "Save Module Settings".
 * Retournez dans l'onglet "Game Settings" puis cliquez sur "Configure Settings" pour ouvrir le menu "Configure Game Settings".
 * Enfin, pour le paramètre "Language Preference", dérouler la liste des langues pour sélectionner "Français", puis cliquez sur "Save Changes".
 * Voici votre monde de Warhammer en français !

## Une erreur à faire remonter, une suggestion ?

Contactez-moi par l'un des biais sur mon profil GitLab ou faites remonter un bogue par l'interface de GitLab.

## Compatibilité

 * Foundry : 0.5.4+
 * Warhammer v4 : v1.2+
 * Babele : 1.9+

## Commentaire

Toute suggestion ou retour est apprécié, vous pouvez me contacter par les mêmes biais que ceux mentionnés plus haut.

## Remerciements

 * Merci à Moo Man pour son superbe travail sur le système de jeu pour WFRP4.
 * Merci à @Tuckel pour la traduction initiale des textes principaux, des tables et du compendium.
 * Merci à tous les traducteurs : Celewyr, Moilu, Gharazel, Jerôme Paire, Baptiste Delanoue, Dr. Droide, Gharazel.

## Licence

WFRP4e - Traduction française is a module for Foundry VTT by LeRatierBretonnien and is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).

Le système Warhammer 4 et tout ses contenus sont la propriété de Cubicle7, sous Licence de Games Workshop, et tout les droits leur reviennent naturellement. 
La traduction française de Khaos Project qui a servi de base à cette traduction, est à créditer pour tout les termes et items traduits, ce qui reste leur entière propriété, naturellement également.

All original material belongs to the aforementioned authors. No infringement intended.

This module is completely unofficial and in no way endorsed by Games Workshop Limited. Chaos, the Chaos device, the Chaos logo, Citadel, Citadel Device, Darkblade, the Double-Headed/ Imperial Eagle device, ‘Eavy Metal, Forge World, Games Workshop, Games Workshop logo, Golden Demon, Great Unclean One, GW, the Hammer of Sigmar logo, Horned Rat logo, Keeper of Secrets, Khemri, Khorne, the Khorne logo, Lord of Change, Nurgle, the Nurgle logo, Skaven, the Skaven symbol devices, Slaanesh, the Slaanesh logo, Tomb Kings, Trio of Warriors, Twin Tailed Comet Logo, Tzeentch, the Tzeentch logo, Warhammer, Warhammer Online, Warhammer World logo, White Dwarf, the White Dwarf logo, and all associated marks, names, races, race insignia, characters, vehicles, locations, units, illustrations and images from the Warhammer world are either ®, TM and/or © Copyright Games Workshop Ltd 2000-2020, variably registered in the UK and other countries around the world. Used without permission. No challenge to their status intended. All Rights Reserved to their respective owners.
