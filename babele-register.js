Hooks.once('init', () =>
{
	// Babele registering
	if(typeof Babele !== 'undefined')
	{
		Babele.get().register(
		{
			module: "wfrp4e-traduction-francaise",
			lang: "fr",
			dir: "compendium"
		})
	}
	
	Babele.get().registerConverters(
	{
		// Auto-translate generic stuff
		"genericLocalization": (value) =>
		{
			if(value)
				return game.i18n.localize(value);
		},
		
		// Auto-translate careers talents
		"talentConverter": (talents) =>
		{
			if(talents)
			{
				let translations = new Array()
				let compendiumNames = game.wfrp4e.tags.getPacksWithTag("talent").map(t =>  t.metadata.name)
				let translatedCompendiums = game.babele.packs.filter(p => p.translated && compendiumNames.includes(p.metadata.name))
				let re  = /(.*)\((.*)\)/i
				let length = talents.length
				
				for(let i = 0; i < length; i++)
				{
					let talentName = talents[i]
					let specialty
					let res = re.exec(talents[i])
					
					if(res)
					{
						talentName = res[1].trim()
						specialty = res[2].trim()
					}
					
					for(let compendium of translatedCompendiums)
					{
						let talent = compendium.translations[talentName]
						
						if(talent)
						{
							translations[i] = talent.name
							
							if(specialty)
								translations[i] += " (" + game.i18n.localize(specialty) + ")"
							
							break;
						}
					}
				}
				
				return translations.sort(Intl.Collator().compare)
			}
		},

		// Auto-translate spells duration, range, target and damages
		"spellConverter": (value, defaultTranslation) =>
		{
			if(defaultTranslation)
				return defaultTranslation;
			else if(value == "")
				return ""
			else if(value == "Touch")
				return "Contact"
			else if(value == "You")
				return "Vous"
			else if(value == "Instant")
				return "Immédiat"
			else if(value == "Special")
				return "Spécial"

			let translation = value;
			let unit = "";
			
			// Test "<characteristic> Bonus <unit>" pattern
			let re = /(.*) Bonus (\w*)/i
			let res = re.exec(value)
			
			if(res)
			{
				if(res[1])
				{
					// We have char name, then convert it
					translation = "Bonus de " + game.i18n.localize(res[1].trim())
				}
				
				unit = res[2];
			}
			else
			{
				// Test "<number> <unit>" pattern
				re = /(\d+) (\w+)/i
				res = re.exec(value)
				
				if(res)
				{
					translation = res[1]
					unit = res[2]
				}
				else
				{
					// Test "<characteristic> <unit>" pattern
					re = /(\w+) (\w+)/i
					res = re.exec(value)
					
					if(res)
					{
						translation = game.i18n.localize(res[1].trim())
						unit = res[2]
					}
				}
			}
			
			switch(unit)
			{
				case "hour":
					unit = "heure"
				break;
				case "hours":
					unit = "heures"
				break;
				case "days":
					unit = "jours"
				break;
				default:
					unit = game.i18n.localize(unit.trim())
			}
			
			translation += " " + unit
			return translation
		},
		
		// Auto-translate trappings properties
		"trappingProperties": (value) =>
		{
			if(!Array.isArray(value))
			{
				let list = value.split(",")
				let re  = /(.*) (\d+)/i
				
				for(let i = 0; i < list.length; i++)
				{
					let trimmedItem = list[i].trim()
					
					// Test "<property> <rating>" pattern
					let splittedItem = re.exec(trimmedItem)
					
					if(splittedItem)
					{
						list[i] = game.i18n.localize("PROPERTY." + splittedItem[1]) + " " + splittedItem[2]
					}
					else
					{
						list[i] = game.i18n.localize("PROPERTY." + trimmedItem)
					}
				}
				
				return list.toString()
			}
		},
		
		"effectConverter": (effects, translations) =>
		{
			if(translations)
			{
				return effects.map((effect, index) =>
				{
					return mergeObject(effect, translations[index]);
				})
			}
		}
	})
})

Hooks.once('ready', () =>
{
	let config =
	{
		effectApplication:
		{
			"equipped" : "Quand l'objet est équipé",
			"apply" : "Appliquer avec ciblage",
			"damage" : "Appliquer quand l'objet entraîne des Dégâts",
		},
		effectPlaceholder:
		{
			"invoke" :
			`Cet effet n'est appliqué que lorsque le button Invoquer est appuyé.
			args:

			none`,
			"oneTime" : 
			`Cet effet se produit une fois, immédiatement lors de l'application.
			args:

			actor : l'Acteur possédant l'effet.
			`,
			"prefillDialog" : 
			`This effect is applied before rendering the roll dialog, and is meant to change the values prefilled in the bonus section
			args:

			prefillModifiers : {modifier, difficulty, slBonus, successBonus}
			type: string, 'weapon', 'skill' 'characteristic', etc.
			item: the item used of the aforementioned type
			options: other details about the test (options.rest or options.mutate for example)
			
			Example: 
			if (args.type == "skill" && args.item.name == "Athlétisme") args.prefillModifiers.modifier += 10`,

			"prePrepareData" : 
			`This effect is applied before any actor data is calculated.
			args:

			actor : actor who owns the effect
			`,

			"prePrepareItems" : 
			`This effect is applied before items are sorted and calculated

			actor : actor who owns the effect
			`,

			"prepareData" : 
			`This effect is applied after actor data is calculated and processed.

			args:

			actor : actor who owns the effect
			`,

			"preWoundCalc" : 
			`This effect is applied right before wound calculation, ideal for swapping out characteristics or adding multipiliers

			actor : actor who owns the effect
			sb : Strength Bonus
			tb : Toughness Bonus
			wpb : Willpower Bonus
			multiplier : {
				sb : SB Multiplier
				tb : TB Multiplier
				wpb : WPB Modifier
			}

			e.g. for Endurci: "args.multiplier.tb += 1"
			`,

			"woundCalc" : 
			`This effect happens after wound calculation, ideal for multiplying the result.

			args:

			actor : actor who owns the effect
			wounds : wounds calculated

			e.g. for Swarm: "wounds *= 5"
			`,

			"preApplyDamage" : 
			`This effect happens before applying damage in an opposed test

			args:

			actor : actor who is taking damage
			attacker : actor who is attacking
			opposedTest : object containing opposed test data
			damageType : damage type selected (ignore TB, AP, etc.)
			`,
			"applyDamage" : 
			`This effect happens after damage in an opposed test is calculated, but before actor data is updated.

			args:

			actor : actor who is taking damage
			attacker : actor who is attacking
			opposedTest : object containing opposed test data
			damageType : damage type selected (ignore TB, AP, etc.)
			totalWoundLoss : Wound loss after mitigations
			AP : data about the AP used
			updateMsg : starting string for damage update message
			messageElements : arary of strings used to show how damage mitigation was calculated
			`,

			"preTakeDamage" : 
			`This effect happens before taking damage in an opposed test

			args:

			actor : actor who is taking damage
			attacker : actor who is attacking
			opposedTest : object containing opposed test data
			damageType : damage type selected (ignore TB, AP, etc.)
			`,
			
			"takeDamage" : 
			`This effect happens after damage in an opposed test is calculated, but before actor data is updated.

			args:

			actor : actor who is taking damage
			attacker : actor who is attacking
			opposedTest : object containing opposed test data
			damageType : damage type selected (ignore TB, AP, etc.)
			totalWoundLoss : Wound loss after mitigations
			AP : data about the AP used
			updateMsg : starting string for damage update message
			messageElements : arary of strings used to show how damage mitigation was calculated
			`,

			"preApplyCondition" :  
			`This effect happens before effects of a condition are applied.

			args:

			effect : condition being applied
			data : {
				msg : Chat message about the application of the condition
				<other data, possibly condition specific>
			}
			`,

			"applyCondition" :  
			`This effect happens after effects of a condition are applied.

			args:

			effect : condition being applied
			data : {
				messageData : Chat message about the application of the condition
				<other data, possibly condition specific>
			}
			`,
			"prePrepareItem" : 
			`This effect is applied before an item is processed with actor data.

			args:

			item : item being processed
			`,
			"prepareItem" : 
			`This effect is applied after an item is processed with actor data.

			args:

			item : item processed
			`,
			"preRollTest": 
			`This effect is applied before a test is calculated.

			args:

			testData: All the data needed to evaluate test results
			cardOptions: Data for the card display, title, template, etc
			`,
			"preRollWeaponTest" :  
			`This effect is applied before a weapon test is calculated.

			args:

			testData: All the data needed to evaluate test results
			cardOptions: Data for the card display, title, template, etc
			`,

			"preRollCastTest" :  
			`This effect is applied before a casting test is calculated.

			args:

			testData: All the data needed to evaluate test results
			cardOptions: Data for the card display, title, template, etc
			`,

			"preChannellingTest" :  
			`This effect is applied before a channelling test is calculated.

			args:

			testData: All the data needed to evaluate test results
			cardOptions: Data for the card display, title, template, etc
			`,

			"preRollPrayerTest" :  
			`This effect is applied before a prayer test is calculated.

			args:

			testData: All the data needed to evaluate test results
			cardOptions: Data for the card display, title, template, etc
			`,

			"preRollTraitTest" :  
			`This effect is applied before a trait test is calculated.

			args:

			testData: All the data needed to evaluate test results
			cardOptions: Data for the card display, title, template, etc
			`,

			"rollTest" : 
			`This effect is applied after a test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,
			"rollIncomeTest" : 
			`This effect is applied after an income test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,

			"rollWeaponTest" : 
			`This effect is applied after a weapon test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,

			"rollCastTest" : 
			`This effect is applied after a casting test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,

			"rollChannellingTest" : 
			`This effect is applied after a channelling test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,

			"rollPrayerTest" : 
			`This effect is applied after a prayer test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,

			"rollTraitTest" : 
			`This effect is applied after a trait test is calculated.

			args:

			test: object containing test and result information
			cardOptions: Data for the card display, title, template, etc
			`,

			"preOpposedAttacker" : 
			`This effect is applied before an opposed test result begins calculation, as the attacker.

			args:

			attackerTest: test object of the attacker
			defenderTest: test object of the defender
			opposedTest: opposedTest object, before calculation
			`,
			"preOpposedDefender" : 
			`This effect is applied before an opposed test result begins calculation, as the defender.

			args:

			attackerTest: test object of the attacker
			defenderTest: test object of the defender
			opposedTest: opposedTest object, before calculation
			`,

			"opposedAttacker" : 
			`This effect is applied after an opposed test result begins calculation, as the attacker.

			args:

			attackerTest: test object of the attacker
			defenderTest: test object of the defender
			opposedTest: opposedTest object, after calculation
			`,

			"opposedDefender" : 
			`This effect is applied before an opposed test result begins calculation, as the defender.

			args:

			attackerTest: test object of the attacker
			defenderTest: test object of the defender
			opposedTest: opposedTest object, after calculation
			`,

			"calculateOpposedDamage" : 
			`This effect is applied during an opposed test damage calculation. This effect runs on the attacking actor

			args:

			damage : initial damage calculation before multipliers
			damageMultiplier : multiplier calculated based on size difference
			sizeDiff : numeric difference in sized, will then be used to add damaging/impact
			opposedTest : opposedTest object
			`,

			"getInitiativeFormula" : 
			`This effect runs when determining actor's initiative

			args:

			initiative: Calculated initiative value
			`,

			"targetPrefillDialog" : 
			`This effect is applied to another actor whenever they target this actor, and is meant to change the values prefilled in the bonus section
			args:

			prefillModifiers : {modifier, difficulty, slBonus, successBonus}
			type: string, 'weapon', 'skill' 'characteristic', etc.
			item: the item used of the aforementioned type
			options: other details about the test (options.rest or options.mutate for example)
			
			Example: 
			if (args.type == "skill" && args.item.name == "Athlétisme") args.prefillModifiers.modifier += 10`,

			"endTurn" : 
			`This effect runs at the end of an actor's turn

			args:

			combat: current combat
			`,

			"endRound" :  
			`This effect runs at the end of a round

			args:

			combat: current combat
			`,
			"endCombat" :  
			`This effect runs when combat has ended

			args:

			combat: current combat
			`,

			"this" : 
			`
			
			All effects have access to: 
				this.actor : actor running the effect
				this.effect : effect being executed
				this.item : item that has the effect, if effect comes from an item`
		},
		effectTriggers:
		{
			"invoke" : "Invocation manuelle",
			"oneTime" : "Immédiat",
			"dialogChoice" : "Choix de Dialogue",
			"prefillDialog" : "Pré-remplissage de Dialogue",
			"prePrepareData" : "Pré-préparation des Données",
			"prePrepareItems" : "Pré-préparation des Objets de l'Acteur",
			"prepareData" : "Préparation des Données",
			"preWoundCalc" : "Pré-calcul des points de Blessure",
			"woundCalc" : "Calcul des points de Blessure",
			"preApplyDamage" : "Pré-application des Dégâts",
			"applyDamage" : "Application des Dégâts",
			"preTakeDamage" : "Pré-réception des Dégâts",
			"takeDamage" : "Réception des Dégâts",
			"preApplyCondition" : "Pré-application d'un État",
			"applyCondition" : "Application d'un État",
			"prePrepareItem" : "Pré-préparation de l'Objet",
			"prepareItem" : "Préparation de l'Objet",
			"preRollTest" : "Pré-lancer de Test",
			"preRollWeaponTest" : "Pré-lancer de Test d'arme",
			"preRollCastTest" : "Pré-lancer de Test d'incantation",
			"preChannellingTest" : "Pré-lancer de Test de focalisation",
			"preRollPrayerTest" : "Pré-lancer de Test de prière",
			"preRollTraitTest" : "Pré-lancer de Test de Trait",
			"rollTest" : "Lancer de Test",
			"rollIncomeTest" : "Lancer de Test de Revenu",
			"rollWeaponTest" : "Lancer de Test d'arme",
			"rollCastTest" : "Lancer de Test d'incantation",
			"rollChannellingTest" : "Lancer de Test de focalisation",
			"rollPrayerTest" : "Lancer de Test de prière",
			"rollTraitTest" : "Lancer de Test de Trait",
			"preOpposedAttacker" : "Pré-opposition de l'attaquant",
			"preOpposedDefender" : "Pré-opposition du défenseur",
			"opposedAttacker" : "Opposition de l'attaquant",
			"opposedDefender" : "Opposition du défenseur",
			"calculateOpposedDamage" : "Calcul des Dégâts opposés",
			"targetPrefillDialog" : "Pré-remplissage du Dialogue du cibleur",
			"getInitiativeFormula" : "Récupérer l'Initiative",
			"endTurn" : "Fin du tour",
			"endRound" : "Fin de la manche",
			"endCombat" : "Fin du combat"
		},
		hitLocationTables:
		{
			"snake": "Serpent",
			"spider": "Arachnide",
			"quadruped": "Quadrupède",
			"rowingboat": "Bateau à rames",
			"sailingboat": "Bateau à voile"
		},
		loreEffectDescriptions:
		{
			"tzeentch": "<p>Les cibles des sorts du domaine de Tzeentch sont déchirées par la magie transformatrice du Chaos. Les cibles affectées par un sort du domaine de Tzeentch doivent réussir un Test de <strong>@Compendium[wfrp4e-core.skills.CcNJsS4jSwB6Ug1J]{Résistance} Exigeant (+0)</strong> ou prendre +1 point de Corruption. S'ils réussissent leur Test, ils prennent à la place +1 point de Chance, qui peut être utilisé normalement. Tels sont les caprices de Tzeentch.</p>"
		},
		modTypes:
		{
			"hull": "Coque",
			"steering": "Direction",
			"superstructure": "Superstructure",
			"rigging": "Gréement",
			"oars": "Avirons",
			"propulsion": "Propulsion"
		},
		speciesSkills:
		{
			human: ["Calme", "Charisme", "Commandement", "Distance (Arc)", "Évaluation", "Langue (Bretonnien)", "Langue (Wastelander)", "Marchandage", "Mêlée (Basique)", "Ragot", "Savoir (Reikland)", "Soin des animaux"],
			dwarf: ["Calme", "Divertissement (Narration)", "Évaluation", "Intimidation", "Langue (Khazalid)", "Mêlée (Basique)", "Métier (Au choix)", "Résistance", "Résistance à l'alcool", "Savoir (Géologie)", "Savoir (Métallurgie)", "Savoir (Nains)"],
			halfling: ["Charisme", "Discrétion (Au choix)", "Escamotage", "Esquive", "Intuition", "Langue (Mootique)", "Marchandage", "Métier (Cuisinier)", "Pari", "Perception", "Résistance à l'alcool", "Savoir (Reikland)"],
			helf: ["Calme", "Commandement", "Distance (Arc)", "Divertissement (Chant)", "Évaluation", "Langue (Elthárin)", "Mêlée (Basique)", "Musique (Au choix)", "Natation", "Navigation", "Perception", "Pilotage"],
			welf: ["Athlétisme", "Discrétion (Rurale)", "Distance (Arc)", "Divertissement (Chant)", "Escalade", "Intimidation", "Langue (Elthárin)", "Mêlée (Basique)", "Perception", "Pistage", "Résistance", "Survie en extérieur"],
			gnome: ["Charisme", "Discrétion (Au choix)", "Divertissement (Au choix)", "Esquive", "Focalisation (Ulgu)", "Langue (Ghassally)", "Langue (Magick)", "Langue (Wastelander)", "Marchandage", "Ragot", "Résistance à l'alcool", "Survie en extérieur"],
		},
		speciesTalents:
		{
			human: ["Affable, Perspicace", "Destinée", 3],
			dwarf: ["Acharné, Lire/écrire", "Costaud", "Déterminé, Inébranlable", "Résistance à la magie", "Vision nocturne", 0],
			halfling: ["Petit", "Résistance (Chaos)", "Sens aiguisé (Goût)", "Vision nocturne", 2],
			helf: ["Flegmatique, Perspicace", "Lire/écrire", "Seconde vue, Sixième sens", "Sens aiguisé (Vue)", "Vision nocturne", 0],
			welf: ["Endurci, Seconde vue", "Lire/écrire, Très résistant", "Nomade", "Sens aiguisé (Vue)", "Vision nocturne", 0],
			gnome: ["Chanceux, Imitation", "Imprégné d'Ulgu, Incognito", "Lire/écrire, Pêcheur", "Petit", "Seconde vue, Sixième sens", "Vision nocturne", 0],
		},
		subspecies:
		{
			human:
			{
				"averlander-nom":
				{
					name: "Averlander",
					skills: ["Commandement", "Divertissement (Discours)", "Emprise sur les animaux", "Équitation (Cheval)", "Langue (Bretonnien)", "Langue (Khazalid)", "Langue (Mootique)", "Mêlée (Basique)", "Métier (Fermier)", "Ragot", "Savoir (Averland)", "Soin des animaux"],
					talents: ["Affable, Affûté", "Destinée, Haine (Peaux-vertes)", "Étiquette (Nains), @Table[talents]{1 Talent aléatoire}", 2]
				},
				"hochlander-nom":
				{
					name: "Hochlander",
					skills: ["Calme", "Charisme", "Discrétion (Rurale)", "Distance (Arc ou Poudre noire)", "Marchandage", "Mêlée (Basique)", "Perception", "Pistage", "Savoir (Hochland)", "Soin des animaux", "Survie en extérieur", "Symboles secrets (Chasseurs)"],
					talents: ["Affûté, Fin tireur", "Destinée, Nomade", 3]
				},
				"ostermarker-nom":
				{
					name: "Ostermarker",
					skills: ["Calme", "Commandement", "Distance (Arc)", "Intimidation", "Intuition", "Langue (Gospodarin)", "Langue (Ungol)", "Marchandage", "Mêlée (Basique ou Hast)", "Résistance", "Résistance à l'alcool", "Savoir (Ostermark)"],
					talents: ["Destinée, @Table[talents]{1 Talent aléatoire}", "Fin tireur, Flegmatique", 3]
				},
				"ostlander-nom":
				{
					name: "Ostlander",
					skills: ["Calme", "Intimidation", "Langue (Gospodarin)", "Langue (Ungol)", "Mêlée (Basique)", "Natation", "Navigation", "Pari", "Pilotage", "Résistance", "Savoir (Ostland)", "Survie en extérieur"],
					talents: ["Destinée, @Table[talents]{1 Talent aléatoire}", "Très résistant, Soupe au caillou", 3]
				},
				reiklander:
				{
					name: "Reiklander",
					skills: ["Calme", "Charisme", "Commandement", "Distance (Arc)", "Évaluation", "Langue (Bretonnien)", "Langue (Wastelander)", "Marchandage", "Mêlée (Basique)", "Ragot", "Savoir (Reikland)", "Soin des animaux"],
					talents: ["Affable, Perspicace", "Destinée", 3],
				},
				"stirlander-nom":
				{
					name: "Stirlander",
					skills: ["Calme", "Commandement", "Discrétion (Rurale ou Urbaine)", "Distance (Arc)", "Escamotage", "Évaluation", "Langue (Mootique)", "Marchandage", "Mêlée (Basique)", "Ragot", "Savoir (Stirland)", "Subornation"],
					talents: ["Affable, Doigts de fée", "Destinée, @Table[talents]{1 Talent aléatoire}", 3]
				},
				"sylvanian-nom":
				{
					name: "Sylvanien",
					skills: ["Athlétisme", "Calme", "Charisme", "Discrétion (Rurale)", "Emprise sur les animaux", "Mêlée (Basique ou Hast)", "Résistance", "Savoir (Morts-vivants)", "Savoir (Sylvanie)", "Savoir (Vampires)", "Soin des animaux", "Survie en extérieur"],
					talents: ["Affûté, Flegmatique", "Fuite !", 3]
				},
				"talabeclander-nom":
				{
					name: "Talabeclander",
					skills: ["Calme", "Distance (Arc)", "Emprise sur les animaux", "Marchandage", "Mêlée (Basique)", "Navigation", "Perception", "Pistage", "Ragot", "Savoir (Talabecland)", "Soin des animaux", "Survie en extérieur"
					],
					talents: ["Destinée, Tir précis", "Fin tireur, Perspicace", 3]
				},
				"wissenlander-nom":
				{
					name: "Wissenlander",
					skills: ["Calme", "Commandement", "Distance (Poudre noire)", "Intuition", "Langue (Bretonnien)", "Langue (Estalien ou Tiléen)", "Marchandage", "Mêlée (Basique)", "Métier (Fermier ou Mineur)", "Résistance", "Savoir (Wissenland)", "Soin des animaux"],
					talents: ["Doomed", "Coolheaded, Perspicace", 3]
				},
				"albionite-nom":
				{
					name: "Albionite",
					skills: ["Athlétisme", "Calme", "Discrétion (Rurale)", "Distance (Lancer)", "Escalade", "Intimidation", "Mêlée (Basique)", "Mêlée (Hast)", "Résistance", "Savoir (Albion)", "Savoir (Bêtes ou Monstres)", "Survie en extérieur"],
					talents: ["Guerrier né", "Marcheur (Marais)", "Résistant (Mutation)", "Soupe au caillou", "Très fort Strong, Très résistant"]
				},
				"arabyan-nom":
				{
					name: "Arabien",
					skills: ["Art (Au choix)", "Équitation (Chameau ou Cheval)", "Évaluation", "Langue (Indien ou Sud)", "Langue (Wastelander)", "Marchandage", "Mêlée (Basique)", "Navigation", "Pilotage", "Prière", "Ragot", "Savoir (Cité-État)"],
					talents: ["Affable, Doigts de fée", "Artisan (Au choix), Lire/écrire", "Marcheur (Déserts), Résistant (Chaleur)", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents de la cité-État (Au choix)}"]
				},
				"breton-noble-nom":
				{
					name: "Noble bretonnien",
					skills: ["Calme", "Charisme", "Commandement", "Équitation (Cheval)", "Langue (Au choix)", "Marchandage", "Mêlée (Basique ou À deux mains)", "Prière", "Ragot", "Savoir (Duché)", "Savoir (Politique)", "Soin des animaux"],
					talents: ["Affable, Guerrier né", "Lire/écrire", "Sang noble", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents du duché (Au choix)}"]
				},
				"breton-lowborn-nom":
				{
					name: "Paysan bretonnien",
					skills: ["Calme", "Distance (Arc)", "Emprise sur les animaux", "Équitation (Cheval)", "Marchandage", "Mêlée (Basique)", "Métier (Fermier)", "Ragot", "Résistance", "Savoir (Agriculture)", "Savoir (Duché)", "Soin des animaux"],
					talents: ["Doigts de fée, Très résistant", "Échine solide", "Soupe au caillou", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents du duché (Au choix)}"]
				},
				"cathayan-nom":
				{
					name: "Cathayais",
					skills: ["Art (Au choix)", "Charisme", "Commandement", "Distance (Arc)", "Escalade", "Marchandage", "Mêlée (Basiue)", "Langue (Indan)", "Langue (Nipponais)", "Langue (Wastelander)", "Ragot", "Savoir (Province)"],
					talents: ["Flegmatique, Perspicace", "Linguistique, Lire/écrire", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents provinciaux (Au choix)}"]
				},
				"estalian-nom":
				{
					name: "Estalien",
					skills: ["Athlétisme", "Charisme", "Divertissement (Narration)", "Escamotage", "Langue (Arabien)", "Langue (Bretonnien)", "Langue (Tiléen)", "Représentation (Jota)", "Ragot", "Résistance à l'alcool", "Savoir (Region)", "Subornation"],
					talents: ["Flair pour les ennuis", "Affable, Perspicace", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents régionnaux (Au choix)}"]
				},
				"indan-nom":
				{
					name: "Indien",
					skills: ["Calme", "Charisme", "Distance (Lancer)", "Langue (Arabien)", "Langue (Cathayais)", "Langue (Wastelander)", "Marchandage", "Mêlée (Basique)", "Ragot", "Résistance", "Savoir (Royaume)", "Soin des animaux"],
					talents: ["Affable, Réflexes foudroyants", "Chanceux", "Résistant (Chaleur), @Table[talents]{1 Talent aléatoire}", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents du royaume (Au choix)}"
					]
				},
				"gospodar-nom":
				{
					name: "Gospodar",
					skills: ["Calme", "Distance (Arc)", "Emprise sur les animaux", "Équitation (Cheval)", "Évaluation", "Langue (Norse ou Wastelander)", "Langue (Ungol)", "Marchandage", "Mêlée (Basique)", "Ragot", "Savoir (Kislev)", "Soin des animaux"],
					talents: ["Affable, Perspicace", "Guerrier né, Très fort", "Résistant (Froid)", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents provinciaux (Au choix)}"]
				},
				"ungol-nom":
				{
					name: "Ungol",
					skills: ["Calme", "Conduite", "Distance (Arc)", "Emprise sur les animaux", "Intimidation", "Langue (Gospodarin)", "Marchandage", "Mêlée (Basique)", "Ragot", "Savoir (Kislev)", "Soin des animaux", "Survie en extérieur"],
					talents: ["Endurci, @Table[talents]{1 Talent aléatoire}", "Fin tireur, Très résistant", "Résistant (Froid)", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents provinciaux (Au choix)}"]
				},
				"nipponese-nom":
				{
					name: "Nipponais",
					skills: ["Calme", "Charisme", "Commandement", "Distance (Arc)", "Langue (Cathayais)", "Langue (Wastelander)", "Marchandage", "Mêlée (Basique)", "Natation", "Pilotage", "Ragot", "Savoir (Nippon)"],
					talents: ["Affûté, Guerrier né", "Sixième sens", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents du clan (Au choix)}"]
				},
				"norscan-nom":
				{
					name: "Norse",
					skills: ["Athlétisme", "Calme", "Escalade", "Intimidation", "Mêlée (Basique ou À deux mains)", "Natation", "Perception", "Pilotage", "Résistance", "Résistance à l'alcool", "Savoir (Norsca)", "Survie en extérieur"],
					talents: ["Guerrier né, Très fort", "Résistant (Froid)", "Vision nocturne", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents tribaux (Au choix)}, @Table[talents]{2 Talents aléatoires}"]
				},
				"southlander-nom":
				{
					name: "Southlander",
					skills: ["Athlétisme", "Calme", "Distance (Lancer)", "Escalade", "Langue (Arabien ou Cathayais)", "Mêlée (Hast)", "Navigation", "Perception", "Ragot", "Résistance", "Soin des animaux", "Survie en extérieur"],
					talents: ["Bonnes jambes, @Table[talents]{1 Talent aléatoire}", "Réflexes foudroyants, Sprinteur","@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents tribaux (Au choix)}"]
				},
				"strigany-nom":
				{
					name: "Strigany",
					skills: ["Charisme", "Divertissement (Chant ou Narration)", "Emprise sur les animaux", "Équitation (Cheval)", "Langue (Au choix)", "Marchandage", "Musique (Au choix)", "Ragot", "Savoir (Esprits)", "Savoir (Local)", "Soin des animaux", "Représentation (Au choix)"],
					talents: ["Affable, Affûté", "Affinité avec les animaux, Oreille absolue", "Voyageur aguerri", 2]
				},
				"tilean-nom":
				{
					name: "Tiléen",
					skills: ["Art (Au choix)", "Charisme", "Divertissement (Narration)", "Escamotage", "Langue (Estalien)", "Langue (Bretonnien)", "Pilotage", "Ragot", "Résistance à l'alcool", "Savoir (Cité-État)", "Savoir (Politique)", "Subornation"],
					talents: ["Affable, Affûté", "Lire/écrire, @Table[talents]{1 Talent aléatoire}", "Sociable, @Table[talents]{1 Talent aléatoire}", "@Compendium[nations-of-mankind-wfrp4e.journalentries-nom.siN4XGTmiQTSMBuX]{Talents de la cité-État (Au choix)}"]
				},
				"westerlander-nom":
				{
					name: "Wastelander",
					skills: ["Calme", "Charisme", "Évaluation", "Langue (Arabien ou Bretonnien)", "Langue (Cathayais ou Nipponais)", "Langue (Estalien ou Tiléen)", "Marchandage", "Métier (Au choix)", "Pilotage", "Ragot", "Savoir (Wasteland)", "Subornation"],
					talents: ["Corrupteur, Négociateur", "Affable, Perspicace", 3]
				}
			}
		},
		symptoms:
		{
			"delirium": "Délire",
			"swelling": "Gonflement",
			"gripes": "Colique"
		},
		symptomDescriptions:
		{
			"delirium": "<p>Votre sensibilité va et vient, avec des moments de clarté subitement remplacés par des accès de divagations, d'hallucinations et de terreurs. Faites un Test de <strong>Force Mentale Exigeant (+0)</strong> chaque heure, et consultez la table des <strong>@Table[delirium]{délires}</strong>.</p>",
			"swelling": "<p>Une partie du corps gonfle jusqu'à plusieurs fois sa taille normale, devenant rouge vif ou violette et devenant presque inutilisable. La partie du corps affectée correspond normalement à la localisation d'une plaie ou d'une morsure provoquant le Gonflement, ou le point de contact par lequel une maladie ou une infection s'est immiscée dans le corps.</p><table style='height: auto; width: auto;' border='1'><tbody><tr style='height: auto;'><th style='width: 25%; height: auto; text-align: center;'><p><strong>Localisation</strong></p></th><th style='width: 75%; height: auto; text-align: center;'><p><strong>Effet</strong></p></th></tr><tr style='height: auto;'><td style='width: 25%; height: auto; text-align: center;'><p>Tête</p></td><td style='width: 75%; height: auto; text-align: justify;'><p>Les yeux et la langue enflent, les joues deviennent livides, la mâchoire est maintenue ouverte. Il est impossible de manger, mais des liquides clairs peuvent être sirotés en petites quantités. Tout Test s'appuyant sur la parole est 3 niveaux plus difficile.</p></td></tr><tr style='height: auto;'><td style='width: 25%; height: auto; text-align: center;'><p>Bras</p></td><td style='width: 75%; height: auto; text-align: justify;'><p>Le bras et la main gonflent, les articulations des épaules et du coude ne peuvent plus bouger, et la main devient inutile. Pour la durée, le bras compte comme @Compendium[wfrp4e-core.injuries.MnMZv7ZXoRqoH9dS]{amputé}.</p></td></tr><tr style='height: auto;'><td style='width: 25%; height: auto; text-align: center;'><p>Corps</p></td><td style='width: 75%; height: auto; text-align: justify;'><p>Le corps entier gonfle jusqu'à ce que le malade ne puisse plus porter de vêtements. Tous les Tests impliquant le Mouvement deviennent 3 niveaux plus difficiles.</p></td></tr><tr style='height: auto;'><td style='width: 25%; height: auto; text-align: center;'><p>Jambe</p></td><td style='width: 75%; height: auto; text-align: justify;'><p>La jambe gonfle de façon grotesque, devenant aussi large que la partie la plus large de la cuisse sur toute sa longueur. Le pied est presque indiscernable. Pour la durée, la jambe compte comme @Compendium[wfrp4e-core.injuries.k00PimCWkff11IA0]{amputée}.</p></td></tr></tbody></table>"/*,
			"gripes": ""*/
		},
		symptomTreatment:
		{
			"delirium": "<p>Certaines autorités traitent le délire comme faisant partie d'une fièvre, prescrivant les mêmes mesures. Les remèdes coûtent de quelques sous à quelques pistoles, et seulement 10% sont efficaces.</p><p>Avec le médicament adéquat, un Test de <strong>@Compendium[wfrp4e-core.skills.HXZaV1CJhmTvcAz4]{Soin} Exigeant (+0)</strong> réussi chasse les hallucinations pendant <strong>@Roll[1d10]</strong> heures.</p><p>Il est également courant d'endormir les patients délirants avec des drogues tranquillisantes, comme la Fleur de Lune ou même la Belladone, pour maintenir le patient au calme jusqu'à ce que la pathologie soit passée, les envoyant dans un Sommeil Agité jusqu'à ce qu'ils récupèrent ou meurent.</p>",
			"swelling": "<p>La plupart des traitements consistent à plonger la partie affectée, ou parfois le corps tout entier, dans un bain d'eau glacée pour réduire la chaleur qui accompagne les gonflements. Un Test <strong>Étendu de @Compendium[wfrp4e-core.skills.HXZaV1CJhmTvcAz4]{Soin} Difficile (-20)</strong> nécessitant +3 DR réduit le gonflement de <strong>@Roll[2d10]</strong> heures. Chaque Test prend une heure. Le patient se retrouve avec +1 État @Condition[Exténué] pour chaque Test effectué au cours du processus.</p><p>Certains médecins saignent le patient avec une lame ou des sangsues en lieu et place. Un Test <strong>Étendu de @Compendium[wfrp4e-core.skills.HXZaV1CJhmTvcAz4]{Soin}</strong> réussi nécessitant +4 SL et des Outils de Métier (Médecin) réduit le gonflement de (@Roll[1d10] + Bonus d'Endurance du patient) heures. Chaque Test a une Difficulté de base Impossible (-50) et prend une demi-heure.</p>"/*,
			"gripes": ""*/
		},
		trade:
		{
			cargoTypes:
			{
				"grain": "Céréale",
				"armaments": "Armement",
				"luxuries": "Produits de luxe",
				"metal": "Métal",
				"timber": "Bois d'œuvre",
				"wine": "Vin",
				"brandy": "Eau-de-vie",
				"wool": "Laine"
			},
			qualities:
			{
				"swill":  "Déplorable",
				"passable":  "Passable",
				"average":  "Moyenne",
				"good":  "Bonne",
				"excellent":  "Excellent",
				"topshelf":  "Haut de gamme"
			},
			seasons:
			{
				"spring": "Printemps",
				"summer": "Été",
				"autumn": "Automne",
				"winter": "Hiver"
			}
		},
		weaponGroups:
		{
			"vehicle": "Véhicule"
		}
	}
	
	mergeObject(game.wfrp4e.config, config)
	
	PatchTable("quadruped")
})

function PatchTable(tableName)
{
	let records = FilePicker.browse("data", "modules/wfrp4e-traduction-francaise/tables/").then(folder => folder.files.find(file => file.substring(file.lastIndexOf("/") + 1, file.indexOf(".json")) == tableName)).then(file => fetch(file)).then(r => r.json())
	game.wfrp4e.tables[tableName] = records
}